import {
  GET_ALL_STATUS_REQUEST,
  GET_ALL_STATUS_SUCCESS,
  GET_ALL_STATUS_FAIL,
} from "../Types";

const initialState = {
  status: [],
  loading: false,
};

export const StatusReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_ALL_STATUS_REQUEST: {
      return { ...state, loading: true, status: [] };
    }
    case GET_ALL_STATUS_SUCCESS:
      return { ...state, loading: false, status: payload };
    case GET_ALL_STATUS_FAIL:
      return {
        ...state,
        loading: false,
        error: payload,
      };
    default:
      return state;
  }
};
