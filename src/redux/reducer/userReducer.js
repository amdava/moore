import {
  USER_DATA_REQUEST,
  USER_DATA_SUCCESS,
  USER_DATA_FAIL,
  GET_USER_DATA_REQUEST,
  GET_USER_DATA_SUCCESS,
  GET_USER_DATA_FAIL,
} from "../Types";

const initialState = {
  data: {},
  loading: false,
};

export const UserReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case USER_DATA_REQUEST: {
      return { ...state, loading: true, data: {} };
    }
    case USER_DATA_SUCCESS:
      return { ...state, loading: false, data: payload };
    case USER_DATA_FAIL:
      return {
        ...state,
        loading: false,
        error: payload,
      };
    default:
      return state;
  }
};

const getinitialState = {
  users: {},
  loading: false,
};

export const UsersReducer = (state = getinitialState, { type, payload }) => {
  switch (type) {
    case GET_USER_DATA_REQUEST: {
      return { ...state, loading: true, users: {} };
    }
    case GET_USER_DATA_SUCCESS:
      return { ...state, loading: false, users: payload };
    case GET_USER_DATA_FAIL:
      return {
        ...state,
        loading: false,
        error: payload,
      };
    default:
      return state;
  }
};
