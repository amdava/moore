import { GET_ALL_STATUS_REQUEST, GET_ALL_STATUS_SUCCESS, GET_ALL_STATUS_FAIL } from "../Types";
import { db } from "../../firebase";

export const getallstatus = () => async (dispatch) => {
	try {
		dispatch({ type: GET_ALL_STATUS_REQUEST });
		const Arr = [];
		await db.ref("status").on("value", (snapshot) => {
			snapshot.forEach((childsnap) => {
				Arr.push(childsnap);
			});
		});
		dispatch({ type: GET_ALL_STATUS_SUCCESS, payload: Arr });
	} catch (error) {
		dispatch({ type: GET_ALL_STATUS_FAIL, payload: error });
	}
};
