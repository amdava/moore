import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { rootReducers } from "./rootstore";
import { composeWithDevTools } from "redux-devtools-extension";

export const store = createStore(
	rootReducers,
	compose(
		applyMiddleware(thunk),
		// composeWithDevTools()
		// window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
	)
);
