import React from "react";
import { Modal, Button, Avatar, Form, Input, InputNumber } from "antd";
import "./modal.css";
import { useSelector } from "react-redux";
import { Spinner } from "../../component/spinner/spinner";
import { isEmpty } from "../../utils/functions/isObjectEmpty";
import { Loader } from "../../component/Loading/Loader";

export const PostMsgModal = ({ visible, modal }) => {
	const { users, loading } = useSelector((state) => state.UsersReducer);
	const onFinish = (values) => {
		console.log("Received values of form: ", values);
	};
	//"https://about.fb.com/wp-content/uploads/2019/11/CompanyInfo_OurCulture_Grace_Image-1.jpg?fit=890%2C668"
	console.log(users);

	return (
		<>
			{!isEmpty(users) && (
				<Spinner loading={loading}>
					<Modal
						className="ModalHandler ant-modal"
						title={
							<div className="flex items-center">
								<Avatar
									className="ImageHandler"
									alt=""
									src={
										users.photo ? (
											users.photo
										) : (
											<div className="md:text-3xl lg:text-6xl">
												{users.userName.charAt(0)}
											</div>
										)
									}
								/>
								&nbsp;&nbsp;&nbsp;
								<span className="Heading">{users.userName.substring(0, 14)}</span>
							</div>
						}
						centered
						footer={null}
						visible={visible}
						closeIcon={<img className="closeImg mt-5" src="img/close.png" alt="" />}
						maskClosable={true}
						onCancel={modal}
					>
						<Form name="user-coins" className="formStyle" onFinish={onFinish}>
							<Form.Item name="coins" initialValue={1}>
								<InputNumber
									min={1}
									max={25}
									className="inputStyle"
									// defaultValue={1}
									placeholder="Send Coins"
								/>
							</Form.Item>
							<div className="labelStyle">Minimum 25 Stars</div>
							<Form.Item
								name="Minimum"
								rules={[
									{
										min: 10,
										max: 100,
									},
								]}
							>
								<Input className="inputStyle" placeholder="Message (Optional)" />
							</Form.Item>
							<div className="labelStyle2">0/200</div>

							<Button type="primary" htmlType="submit" className="modal-buttom">
								Send Tip
							</Button>
						</Form>
					</Modal>
				</Spinner>
			)}
		</>
	);
};
