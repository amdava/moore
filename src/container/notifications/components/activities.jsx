import React from "react";
import { Avatar } from "antd";
import classes from "../notification.module.scss";
export const Activities = ({
  UserName,
  Activity,
  isMentioned,
  Time,
  avatar, 
}) => {
  return (
    <div className={ isMentioned ? classes.ActivityMentioned: classes.ActivitiesHandler}>
      <Avatar src={avatar} className={classes.Avatar} />
      <div className={classes.ActivityNormal}>
        <span>{UserName}</span>
        <span>{Activity}</span>
        <span>{Time}</span>
      </div>
    </div>
  );
};
