import React, { useState } from "react";
import { Form, Input, Button, message, Alert } from "antd";
import { MailOutlined } from "@ant-design/icons";
import classes from "./login.module.css";
import { Link } from "react-router-dom";
import { useAuth } from "../../../contexts/AuthContext";
import { Nav } from "../../../component/nav/nav";
import { Footer } from "../../../component/Footer/Footer";
export const ForgetPassword = () => {
  const { resetPassword } = useAuth();
  const [error, seterror] = useState("");
  const [Loading, setLoading] = useState(false);

  const onFinish = async (values) => {
    try {
      seterror("");
      setLoading(true);
      await resetPassword(values.email);
      message.success("Success: Check your inbox for further instructions");
    } catch (error) {
      seterror("Failed to reset password");
      setLoading(false);
    }
  };
  const FormForgetPass = (
    <Form
      name="forget_pass"
      className={classes.formCss}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
    >
      <h1 className={classes.ResetPassword}>Reset Password</h1>
      <Form.Item
        name="email"
        hasFeedback
        rules={[
          {
            type: "email",
            required: true,
            validateStatus: "success",
            message: "Please input your email!",
          },
        ]}
      >
        <Input
          style={{ border: "none", borderBottom: "2px solid #DDDDDD" }}
          prefix={<MailOutlined className={classes.IconStyle} />}
          placeholder="&nbsp;Email"
        />
      </Form.Item>

      <Form.Item>
        <Button
          size="medium"
          className={classes.Buttons}
          type="primary"
          htmlType="submit"
          disabled={Loading}
        >
          Reset Password
        </Button>
      </Form.Item>
      <div className={classes.AccountInfo}>
        Don't have account?&nbsp;
        <Link to="/signup" style={{ textDecoration: "underline", color: "" }}>
          Sign up
        </Link>
      </div>
    </Form>
  );
  return (
    <>
      <div className={classes.desktopDesign}>
        <Nav />
        <div className={classes.container}>
          <div className={classes.heading}>Reset Password</div>
          <div className={classes.formcontainer}>
            <div className={classes.mobileHeading}>
              <span className={classes.loginselected}>
                <Link to="/login" style={{ color: "white" }}>
                  LOG IN
                </Link>
              </span>
              <span>
                <Link to="/signup" style={{ color: "white" }}>
                  SIGN IN
                </Link>
              </span>
            </div>
            {error ? <Alert message={error} type="error" /> : null}
            {FormForgetPass}
          </div>
        </div>
        <Footer />
      </div>
      <div className={classes.mobileDesign}>
        <div className={classes.bluebackground}></div>

        <div className={classes.heading}>Reset Password</div>
        <div className={classes.formcontainer}>
          <div className={classes.mobileHeading}>
            <span className={classes.loginselected}>
              <Link to="/login" style={{ color: "white" }}>
                LOG IN
              </Link>
            </span>
            <span>
              <Link to="/signup" style={{ color: "white" }}>
                SIGN UP
              </Link>
            </span>
          </div>
          {error ? <Alert message={error} type="error" /> : null}
          {FormForgetPass}
        </div>

        <div className={classes.MobileFooter}>
          By signing up, you are agree with our&nbsp;
          <span style={{ color: "#2BC2D3", borderBottom: "#2BC2D3" }}>
            Terms & Conditions
          </span>
        </div>
      </div>
    </>
  );
};
