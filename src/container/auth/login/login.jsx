import React, { useState } from "react";
import { Form, Input, Button, message, Checkbox, Alert } from "antd";
import { LockOutlined, MailOutlined } from "@ant-design/icons";
import classes from "./login.module.css";
import { Link, useHistory } from "react-router-dom";

import { Nav } from "../../../component/nav/nav";
import { Footer } from "../../../component/Footer/Footer";
// import { Loader } from "../../../component/Loading/Loader";
import { BackDrop } from "../../../component/backdrop/backdrop";

import { useAuth } from "../../../contexts/AuthContext";
import { useDispatch } from "react-redux";
import { getuserdata } from "../../../redux/actions/user";

export const LoginPage = () => {
	const history = useHistory();
	const { login, OnTwitterLogin } = useAuth();
	const [error, seterror] = useState("");
	const [Loading, setLoading] = useState(false);
	const dispatch = useDispatch();

	const OnTwitterSignIn = () => {
		try {
			OnTwitterLogin();
		} catch (error) {
			console.log(error);
		}
	};
  
	const onFinish = (values) => {
		console.log(values);
		try {
			seterror("");
			setLoading(true);
			login(values.email, values.password)
				.then(async (result) => {
					console.log(result.user.uid);
					await dispatch(getuserdata(result.user.uid));

					localStorage.setItem("isLoggedin", 1);
					message.success("Success: You are successfully logged in.");
					setLoading(false);
					history.push("/homepage");
				})
				.catch((error) => {
					console.log(error);
					seterror("Failed to log in");
					setLoading(false);
				});
		} catch (error) {
			console.log(error);
			seterror("Failed to log in");
			setLoading(false);
		}
	};

	const loginPage = (
		<>
			<div className={classes.heading}>LOG IN</div>
			<div className={classes.formcontainer}>
				<div className={classes.mobileHeading}>
					<span className={classes.loginselected}>
						<Link to="/login" style={{ color: "white" }}>
							LOG IN
						</Link>
					</span>
					<span>
						<Link to="/signup" style={{ color: "white" }}>
							SIGN UP
						</Link>
					</span>
				</div>
				{error ? <Alert message={error} type="error" /> : null}
				<Form
					name="login_form"
					className={classes.formCss}
					initialValues={{
						remember: true,
					}}
					onFinish={onFinish}
				>
					<Form.Item
						name="email"
						hasFeedback
						rules={[
							{
								type: "email",
								required: true,
								validateStatus: "success",
								message: "Please input your email!",
							},
						]}
					>
						<Input
							style={{ border: "none", borderBottom: "2px solid #DDDDDD" }}
							prefix={<MailOutlined className={classes.IconStyle} />}
							placeholder="&nbsp;Email"
						/>
					</Form.Item>
					<Form.Item
						name="password"
						rules={[
							{
								required: true,
								message: "Please input your Password!",
							},
						]}
					>
						<Input.Password
							style={{ border: "none", borderBottom: "2px solid #DDDDDD" }}
							prefix={<LockOutlined className={classes.IconStyle} />}
							type="password"
							placeholder="&nbsp;Password"
						/>
					</Form.Item>
					<Form.Item>
						<Form.Item name="remember" valuePropName="checked" noStyle>
							<Checkbox>
								<span className={classes.RememberMe}>Remember me</span>
							</Checkbox>
						</Form.Item>
						&nbsp;
						<Link to="/login/reset-password" className={classes.forgetpass}>
							Forget Password?
						</Link>
					</Form.Item>
					<Form.Item>
						<Button
							size="medium"
							className={classes.Buttons}
							type="primary"
							htmlType="submit"
							disabled={Loading}
						>
							Login
						</Button>
					</Form.Item>
					<div className={classes.AccountInfo}>
						Don't have account?&nbsp;
						<Link to="/signup" style={{ textDecoration: "underline", color: "" }}>
							Sign up
						</Link>
					</div>
				</Form>
				&nbsp;
				<div style={{ fontWeight: "bold", color: "#2BC2D3" }}>OR</div>
				&nbsp;
				<Button
					className={classes.ButtonsTwitter}
					size="medium"
					type="primary"
					htmlType="submit"
					onClick={OnTwitterSignIn}
				>
					<img src="/img/twitter-white.png" alt="" />
					&nbsp;
					<span>LOG IN WITH TWITTER</span>
				</Button>
			</div>
		</>
	);
	return (
		<>
			<BackDrop loading={Loading} />
			<div className={classes.desktopDesign}>
				<Nav />
				<div className={classes.container}>{loginPage}</div>
				<Footer />
			</div>
			<div className={classes.mobileDesign}>
				<div className={classes.bluebackground}></div>
				{loginPage}
				<div className={classes.MobileFooter}>
					By signing up, you are agree with our&nbsp;
					<span style={{ color: "#2BC2D3", borderBottom: "#2BC2D3" }}>Terms & Conditions</span>
				</div>
			</div>
		</>
	);
};
