import React, { useEffect, useState } from "react";
import { Card, Avatar, Image, Row, Col } from "antd";
import { useHistory } from "react-router-dom";
import classes from "./post.module.css";
import { Spinner } from "../../../../component/spinner/spinner";
import "./fixes.css";
import { useSelector } from "react-redux";
import { db } from "../../../../firebase";
import { HeartOutlined, HeartFilled } from "@ant-design/icons";
// import firebase from "firebase";

const { Meta } = Card;
export const Post = ({ userData, modal }) => {
	const { data, loading } = useSelector((state) => state.UserReducer);
	const [totalLikes, settotalLikes] = useState([]);
	const [Liked, setLiked] = useState(false);
	const history = useHistory();

	const {
		id,
		title,
		createdBy,
		createdAt,
		text,
		avatar,
		img,
		location,
		uid,
		Likes,
		comments,
	} = userData;

	useEffect(() => {
		db.ref("Likes")
			.child(id)
			.on("value", async (snapshot) => {
				const arr = [];
				await snapshot.forEach((like) => {
					arr.push(like.val());
				});
				settotalLikes(arr);
			});
	}, []);

	useEffect(() => {
		if (id !== undefined && data.uid !== undefined) {
			db.ref("Likes")
				.child(id)
				.child(data.uid)
				.on("value", (snapshot) => {
					if (snapshot.exists()) {
						setLiked(true);
					} else {
						setLiked(false);
					}
				});
		}
	}, [Likes,data.uid]);

	const AddLike = () => {
		db.ref("Likes")
			.child(id)
			.child(data.uid)
			.get()
			.then((snapshot) => {
				if (snapshot.exists()) {
					db.ref("Likes").child(id).child(data.uid).remove();
					setLiked(false);
				} else {
					db.ref("Likes")
						.child(id)
						.child(data.uid)
						.set({ likebyuid: data.uid, likedby: data.userName });
					setLiked(true);
				}
			});
	};

	const OpenUserTimeline = () => {
		history.push();
	};

	return (
		<Spinner loading={loading}>
			<Card className={classes.post}>
				<Meta
					className={classes.posthandler}
					avatar={
						<Avatar src={avatar} className={classes.avatarStyle}>
							{avatar ? null : (
								<div className="text-normal sm:text-3xl w-full sm:p-2">{createdBy.charAt(0)}</div>
							)}
						</Avatar>
					}
					title={
						<div className={classes.CardHeader}>
							<span
								style={{
									display: "flex",
									flexDirection: "column",
									alignItems: "left",
								}}
							>
								<span className={classes.UserName} onClick={OpenUserTimeline}>
									{createdBy}
								</span>
								<span className={classes.UserAddress} onClick={OpenUserTimeline}>
									@{createdBy.split(" ")}
								</span>
							</span>
							<span className={classes.PostDate}>{createdAt}</span>
							<span className={classes.PostArrow}>
								<Image src="img/down.png" alt="" />
							</span>
						</div>
					}
					description={
						<div className={classes.PostData}>
							<div className={classes.PostDes}>{text}</div>

							{img ? (
								<Spinner loading={img !== undefined ? false : true}>
									<div className={`${classes.PostMedia}`}>
										<Image
											preview={true}
											alt="example"
											src={img}
											className=".ant-image-preview-wrap .ant-image-preview-img-wrapper .ant-image-preview-img"
										/>
									</div>
								</Spinner>
							) : null}
							<Row justify="space-between">
								<Col>
									<img src="img/dollar.png" alt="" onClick={() => modal(uid)} />
								</Col>
								<Col>
									<Row justify="space-between" style={{ width: "118px" }}>
										<Col className="flex">
											<span className={classes.postText}>100</span>
											&nbsp;&nbsp;
											<span>
												<img src="img/21.png" alt="" />
											</span>
										</Col>
										<Col className="flex items-center justify-center">
											<span className={classes.postText}>{totalLikes.length}</span>
											&nbsp;&nbsp;
											<span onClick={() => AddLike(id)}>
												{Liked ? (
													<HeartFilled
														style={{ fill: "red", color: "red" }}
														className="text-xl hover:text-blue-600 overflow-hidden"
													/>
												) : (
													<HeartOutlined className="text-xl  overflow-hidden" />
												)}
											</span>
										</Col>
									</Row>
								</Col>
							</Row>
						</div>
					}
				/>
			</Card>
		</Spinner>
	);
};
