import classes from "./status.module.css";
import React, { useState, useEffect } from "react";
import { Avatar, Spin, Carousel } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { store, storage, db } from "../../../../firebase";
import { getallstatus } from "../../../../redux/actions/status";
import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
import KeyboardArrowLeftIcon from "@material-ui/icons/KeyboardArrowLeft";
import plus from "./plus.png";
import { CarouselFn } from "../../../../component/carousel/carousel";
import { isEmpty } from "../../../../utils/functions/isObjectEmpty";

export const Status = () => {
	const { data, loading } = useSelector((state) => state.UserReducer);
	const [isWatch, setisWatch] = useState(false);
	const [slideData, setslideData] = useState();
	const { status } = useSelector((state) => state.StatusReducer);
	const dispatch = useDispatch();

	useEffect(() => {
		dispatch(getallstatus());
	}, [loading]);

	const onViewStatus = async (arr) => {
		console.log(arr);
		await setslideData(arr);
		OpenStatus();
	};

	const OpenStatus = () => {
		setisWatch(!isWatch);
	};

	const AddStatus = (e) => {
		e.preventDefault();
		var file = e.target.files[0];
		if (file !== undefined) {
			var storageref = storage.ref(`status/${data.uid}/${file.name + Date.now()}`);
			var task = storageref.put(file);
			task.on(
				"state_changed",
				(snapshot) => {
					//progresss
				},
				(error) => {
					//incomplete
				},
				async () => {
					await storageref.getDownloadURL().then(async (url) => {
						const id = Date.now() + data.uid;
						await db
							.ref("status")
							.child(data.uid)
							.child(id)
							.set({ url: url, id: id, uid: data.uid, createdAt: Date.now() })
							.then((res) => {
								dispatch(getallstatus());
								store.collection("users").doc(data.uid).update({ haveStatus: true });
							});
					});
				}
			);
		}
	};
	//className={classes.statusStyle}

	function SampleNextArrow(props) {
		const { className, style, onClick } = props;
		return (
			<KeyboardArrowRightIcon
				onClick={onClick}
				className={`${className} ml-5`}
				style={{ color: "black" }}
			/>
		);
	}

	function SamplePrevArrow(props) {
		const { className, style, onClick } = props;
		return (
			<KeyboardArrowLeftIcon onClick={onClick} className={className} style={{ color: "black" }} />
		);
	}

	return (
		<div className={classes.statusStyle}>
			{loading || isEmpty(data) || data.haveStatus === null || status.length === 0 ? (
				<div>Loading</div>
			) : (
				<>
					<Carousel
						className={classes.statusSlider}
						accessibility={true}
						dots={false}
						arrows={true}
						draggable={true}
						swipeToSlide={true}
						// autoplay={true}
						// slidesToScroll={3}
						infinite={true}
						initialSlide={0}
						slidesToShow={5}
						touchMove={true}
						nextArrow={<SampleNextArrow />}
						prevArrow={<SamplePrevArrow />}
					>
						{data.haveStatus === false ? (
							<div className={classes.userStatus}>
								<Avatar
									src={
										data.photo ? (
											data.photo
										) : (
											<div className="md:text-3xl lg:text-5xl">{data.userName.charAt(0)}</div>
										)
									}
									alt=""
									className={classes.ImageStatus}
								/>
								<span className={`${classes.ImageButton} flex items-center justify-center`}>
									<div className={classes.uploadbtnwrapper}>
										<img src={plus} alt="" className={classes.PlusImageStyle} />
										<input type="file" name="myfile" onChange={AddStatus} />
									</div>
								</span>
							</div>
						) : (
							status.map((item, index) => {
								//7
								let arr = [];
								item.forEach((subchildsnap) => {
									if (subchildsnap.val().uid === data.uid) {
										arr.push(subchildsnap.val());
									}
								});
								if (arr.length === 0) return;
								if (arr[0].uid === data.uid) {
									return (
										<span key={index}>
											<div className={classes.userStatus}>
												<Avatar
													src={arr[0].url}
													onClick={() => onViewStatus(arr)}
													alt=""
													className={classes.ImageStatus}
												/>
												<span className={`${classes.ImageButton} flex items-center justify-center`}>
													<div className={classes.uploadbtnwrapper}>
														<img src={plus} alt="" className={classes.PlusImageStyle} />
														<input type="file" name="myfile" onChange={AddStatus} />
													</div>
												</span>
											</div>
										</span>
									);
								}
							})
						)}

						{status.map((item, index) => {
							let arr = [];
							item.forEach((subchildsnap) => {
								if (subchildsnap.val().uid !== data.uid) arr.push(subchildsnap.val());
							});
							if (arr.length === 0) return;
							if (arr[0].uid !== data.uid) {
								return (
									<span key={index}>
										<Avatar
											onClick={() => onViewStatus(arr)}
											src={arr[0].url}
											alt=""
											className={classes.ImageStatus}
										/>
									</span>
								);
							}
						})}
					</Carousel>
					{isWatch ? <CarouselFn isWatch={isWatch} toggle={OpenStatus} data={slideData} /> : null}
				</>
			)}
		</div>
	);
};
