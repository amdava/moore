import React from "react";
import { Home } from "./Home";
import { Home2 } from "./Home2";
import { Home3 } from "./Home3";
import { Home4 } from "./Home4";
import { Home5 } from "./Home5"; 
import { HomeFooter } from "./HomeFooter";
import { Nav } from "../../component/nav/nav";
export const LandingPage = () => {
  return (
    <>
      <Nav />
      <Home />
      <Home2 />
      <Home3 />
      <Home4 />
      <Home5 />
      <div
        style={{
          backgroundColor: "#2BC2D3",
          width: "100%",
          height: "80px",
          borderTop: "1px solid #707070",
        }}
      ></div>
      <HomeFooter />
      <div
        style={{
          backgroundColor: "#2BC2D3",
          width: "100%",
          height: "80px",
          borderTop: "1px solid #707070",
        }}
      ></div>
    </>
  );
};
