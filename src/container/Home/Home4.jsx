import React from "react";
import classes from "./Home4.module.css";
import { Heading } from "./components/Heading";
export const Home4 = () => {
  return (
    <div className={classes.HomeContainer}>
      <Heading heading="Why people love UvUew " />
      <div className={classes.cardContainer}>
        <div className={classes.Card}>
          <div className={classes.img}></div>
          <div className={classes.paragraph}>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad
            minim veniam.
          </div>
          <span>
            <div className={classes.Name}>Sean Farmer</div>
            <div className={classes.status}>Designer</div>
          </span>
        </div>
        <div className={classes.Card}>
          <div className={classes.img2}></div>
          <div className={classes.paragraph}>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad
            minim veniam.
          </div>
          <span>
            <div className={classes.Name}>Victor Cray</div>
            <div className={classes.status}>Dancer</div>
          </span>
        </div>
        <div className={classes.Card}>
          <div className={classes.img3}></div>
          <div className={classes.paragraph}>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad
            minim veniam.
          </div>
          <span>
            <div className={classes.Name}>Leroy Rice</div>
            <div className={classes.status}>Artist</div>
          </span>
        </div>
      </div>
    </div>
  );
};
