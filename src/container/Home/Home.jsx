import classes from "./Home.module.css";
import { Button } from "antd";
import React from "react";

export const Home = () => {
  return (
    <div className={classes.HomeContainer}>
      <div className={classes.TextContainer}>
        <h1 className={classes.Heading}>Welcome to UvUew</h1>
        <h1 className={classes.SubHeading}>Earn profits with your content.</h1>
        <p className={classes.Paragraph}>
          Earn profits with your content. UvUew is designed to help content
          creators on the web, create a free account and start making money now.
        </p>
        <Button size="large" type="primary" className={classes.ButtonDesktop}>START MOVE</Button>
      </div>
      <div className={classes.ImgContainer}></div>
      <Button size="large" type="primary" className={classes.ButtonMobile}>START MOVE</Button>
    </div>
  );
};
