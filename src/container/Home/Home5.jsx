import React from "react";
import classes from "./Home5.module.css"; 
import { Link } from 'react-router-dom'
export const Home5 = () => {
  return (
    <div className={classes.HomeContainer}>
      <div className={classes.datahandler}>
        <div className={classes.headingStyle}>
            Download the App
        </div>
        <p className={classes.paragraph}>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad
          minim.
        </p>
        <div className={classes.Buttons}>
        <Link to="/"><img src="./img/9.png" alt=""/></Link>
        <Link to="/"><img src="./img/10.png" alt=""/></Link>
        </div>
      </div>
      <div className={classes.imghandler}>
        <img src="/img/8.png" width="100%" height="100%" alt="" />
      </div>
      <div className={classes.ButtonsMobile}>
        <Link to="/"><img src="./img/9.png" alt=""/></Link>
        <Link to="/"><img src="./img/10.png" alt=""/></Link>
        </div>
    </div>
  );
};
