import classes from "./BankAddStars.module.scss";
import React , { useState }from "react";
import { Button } from "antd";
import { Header } from "../../../../../mobileReuseable/blueheader/header";

import { Card } from "./card";
export const BankAddStarContent = () => {
  const [stars, setstars] = useState(0)
  const OnSubmitForm = () => {
    setstars(AddStars);
  };
  const AddStars = (value) => {
    console.log("stars", value);
    setstars(value) 
  };
  return (
    <>
      <Header heading="Add Stars to Balance" backTo="/settings"/>
      <div className={classes.Container}>
        <div className={classes.text}>
          Stars you purchase here are kept in your balance. You can send Stars
          from your balance anytime.
        </div>
        <div className={classes.textblue}>Learn more about Stars.</div>
        <div className={classes.button}>
          <span>Star Balance: {stars}</span>
          <img src="/img/star.png" alt="" />
        </div>
        <div>
          <img src="/img/warn.png" alt="" className={classes.warnIcon}/>
          <span className={classes.text}>
            &nbsp; You need to buy more stars in order to send 50 Stars.
          </span>
        </div>
        <div className={classes.cardSelected}>
          <span>99 stars Stars</span>
          <span>$0.99</span>
        </div>
        <div className={classes.text}>
          Buy Stars at a discounted price for a limited time.
        </div>
        <Card
          stars="95"
          dollar="1.99"
          bg="white"
          border="#BCC5D3"
          AddStars={() => AddStars(95)}
        />
        <Card
          stars="250"
          dollar="4.99"
          bg="white"
          border="#BCC5D3"
          AddStars={() => AddStars(250)}
        />
        <Card
          stars="530"
          dollar="9.99"
          bg="white"
          border="#BCC5D3"
          AddStars={() => AddStars(530)}
        />
        <Card
          stars="1200"
          dollar="19.99"
          bg="white"
          border="#BCC5D3"
          AddStars={() => AddStars(1200)}
        />
        <Card
          stars="3000"
          dollar="49.99"
          bg="white"
          border="#BCC5D3"
          AddStars={() => AddStars(3000)}
        />
        <Card
          stars="62,00"
          dollar="99.99"
          bg="white"
          border="#BCC5D3"
          AddStars={() => AddStars(6200)}
        />

        <Button className={classes.ButtonStyle} onClick={OnSubmitForm}>
          <img src="/img/confirm.png" alt="" />
          &nbsp;&nbsp;
          <span>SAVE CHANGES</span>
        </Button>
      </div>
    </>
  );
};
