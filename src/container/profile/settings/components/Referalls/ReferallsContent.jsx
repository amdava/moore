import classes from "./Referalls.module.scss";
import React from "react";
import { Header } from "../../../../../mobileReuseable/blueheader/header";
import { Switch, Button } from "antd";
import { Link } from "react-router-dom";

export const ReferallsContent = () => {
  function onChange(checked) {
    console.log(`switch to ${checked}`);
  }

  const OnSubmitForm = () => {
    console.log("");
  };

  return (
    <>
      <Header heading="Referals" />
      <div className={classes.RefContainer}>
        <div className={classes.Refheadingtext}>
          Reward for subscriber referrals
        </div>
        <div className={classes.Refheading2text}>
          <span>1 Free month for referrer</span> <Switch onChange={onChange} />
        </div>
        <div className={classes.Refparagraphtext}>
          Your Fans will be able to share your profile link and for every new
          subscriber they refer to you, your Fans will receive an additional 1
          month subscription to your profile.
        </div>
        <div className={classes.RefTab}>
          <span className={classes.RefTab1}>
            <div>Following price</div>
            <div className={classes.RefTabSubText}>Free</div>
          </span>
          <Link to="/settings/add-stars-to-balance"><img src="/img/arrow-icon.png" width="7.86px" height="13.62px" alt=""/></Link>
        </div>

        <Button className={classes.ButtonStyle} onClick={OnSubmitForm}>
          <img src="/img/confirm.png" alt="" />
          &nbsp;&nbsp;
          <span>SAVE CHANGES</span>
        </Button>
      </div>
    </>
  );
};
