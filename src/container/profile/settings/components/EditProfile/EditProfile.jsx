import React from "react";
import { Settings } from "../../settings";
export const EditProfile = () => {
  return (
    <Settings
      Profile={true}
      EditPage={true}
      PageOpened = {true}
    ></Settings>
  );
};
