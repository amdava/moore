import React, { useState, useEffect } from "react";
import classes from "./profile.module.scss";
import { Header } from "./components/header";
import { Row, Col, Button, Avatar, Affix } from "antd";

import { Post } from "./components/posts/post";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { store } from "../../../firebase";

export const Profile = ({ newpost, ActiveSub, ActivePost, ActiveAbout }) => {
	const [posts, setposts] = useState([]);
	const [top, setTop] = useState(10);

	const { data } = useSelector((state) => state.UserReducer);
	const [tab, settab] = useState(3);

	useEffect(() => {
		store
			.collection("posts")
			.where("email", "==", data.email)
			.onSnapshot((res) => {
				const userPosts = res.docs.map((doc) => doc.data());
				setposts(userPosts);
			});
	}, []);

	const EnableTab = (value) => {
		settab(value);
	};
	const [TabItemActive, setTabItemActive] = useState(1);
	return (
		<div className={`${newpost ? classes.Editedcontainer : classes.container} min-h-screen`}>
			<Header enableheader={true} data={data} />
			<Row justify="space-around" className={classes.RowStyle}>
				<Col className={classes.ColStyle}>
					<span style={{ color: "white" }}>Nothing</span>
					<Link to={"/profile"}>
						<span
							className={classes.heading}
							style={ActiveAbout ? { borderBottom: "5px solid #2BC2D3" } : null}
							onClick={() => EnableTab(1)}
						>
							About
						</span>
					</Link>
				</Col>
				<Col className={classes.ColStyle}>
					<span className={classes.status}>1225</span>
					<Link to={"/profile"}>
						<span
							className={classes.heading}
							style={ActiveSub ? { borderBottom: "5px solid #2BC2D3" } : null}
							onClick={() => EnableTab(2)}
						>
							Subscribers
						</span>
					</Link>
				</Col>
				<Col className={classes.ColStyle}>
					<span className={classes.status}>44</span>
					<Link to={"/profile"}>
						<span
							className={classes.heading}
							style={ActivePost ? { borderBottom: "5px solid #2BC2D3" } : null}
							onClick={() => EnableTab(3)}
						>
							Posts
						</span>
					</Link>
				</Col>
			</Row>
			{tab === 2 ? (
				<>
					{posts.map((item, index) => {
						return (
							<div className={classes.subcard} key={index}>
								<Avatar src={item.avatarimg} alt="" className={classes.subcardAvatar} />
								<div className={classes.subcardheading}>{item.Username}</div>
								<div className={classes.subcardtime}>3 hours ago</div>
							</div>
						);
					})}
					<Button className={classes.ButtonStyle}>
						<img src="/img/add-mentor-icon.png" alt="" />
						&nbsp;&nbsp;
						<span>Take Subscriptions</span>
					</Button>
				</>
			) : tab === 1 ? (
				<div className={classes.AboutHandler}>
					<div className={classes.Aboutheading}>
						Sharing my experience with aspiring photographers
					</div>
					<div className={classes.Aboutparagraph}>
						Hello, my name’s Lucy Lee. I’m a photographer from New York, and have been in the
						industry for over 6 years. I specialise in landscape photography, and am passionate
						about sharing everything that I’ve learned on my photographic journey over the last 6
						years. Ultimately, my goal is to help teach you everything about photography and to
						develop your skills from an absolute beginner, to professional photographer.
					</div>
					<div className={classes.Aboutheading}>Key features</div>
					<div className={classes.Aboutparagraph2}>
						<img src="/img/bullet.png" alt="" />
						<div>
							You will learn how to develop your career and make money as a professional
							photographer.
						</div>
					</div>
					<div className={classes.Aboutparagraph2}>
						<img src="/img/bullet.png" alt="" />
						<div>
							You will learn how to take awesome photos that impress your family, friends and
							colleagues.
						</div>
					</div>
					<div className={classes.Aboutparagraph2}>
						<img src="/img/bullet.png" alt="" />
						<div>
							You will learn how to compose images by following the essential rules of photography.
						</div>
					</div>
					<Button className={classes.ButtonStyle}>
						<img src="/img/add-mentor-icon.png" alt="" />
						&nbsp;&nbsp;
						<span>Take Subscriptions</span>
					</Button>
				</div>
			) : (
				<div>
					{posts.map((item, index) => {
						return <Post key={index} userData={item} />;
					})}
					<Affix offsetTop={top}>
						<Link to="/newpost" className={classes.NewPostButton}>
							<img src="img/plus.png" alt="" />
						</Link>
					</Affix>
				</div>
			)}
		</div>
	);
};
