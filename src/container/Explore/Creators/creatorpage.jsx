import React, { useState } from "react";
import classes from "./style.module.scss";
import { Header } from "../../../mobileReuseable/blueheader/header";
import { Card } from "./card";
import { Nav } from "../../../component/nav/pages/nav";
import { Footer } from "../../../component/Footer/Footer";
export const Creatorpage = () => {
  const [creators, setcreators] = useState([
    {
      Creatorname: "Sara Dravid",
      Creatorstatus: "Developer",
      Creatorimg:
        "https://ak.picdn.net/offset/photos/5b02cf9517fb156e48080fed/medium/photo.jpg",
    },
    {
      Creatorname: "Willian M",
      Creatorstatus: "Painter",
      Creatorimg:
        "https://hips.hearstapps.com/hbz.h-cdn.co/assets/17/15/hbz-hottest-men-george-clooney-gettyimages-178273260.jpg?crop=1.0xw:1xh;center,top&resize=480:*",
    },
    {
      Creatorname: "Mark Wood",
      Creatorstatus: "Photographer",
      Creatorimg:
        "https://images.unsplash.com/photo-1581382575275-97901c2635b7?ixid=MXwxMjA3fDB8MHxzZWFyY2h8Mnx8bWFufGVufDB8fDB8&ixlib=rb-1.2.1&w=1000&q=80",
    },
    {
      Creatorname: "Elina Doe",
      Creatorstatus: "Designer",
      Creatorimg:
        "https://images.pexels.com/photos/1181424/pexels-photo-1181424.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500",
    },
    {
      Creatorname: "Mina Meyer",
      Creatorstatus: "Developer",
      Creatorimg:
        "https://mc.webpcache.epapr.in/mcms.php?size=medium&in=https://mcmscache.epapr.in/post_images/website_154/post_18868303/thumb.jpg",
    },
  ]);
  return (
    <>
      <Nav />
      <div className={classes.container}>
        <Header
          backTo="/homepage"
          forwardTo="/explore"
          heading="Explore Our Creators"
          imgLeft="img/arrow-left.png"
          imgRight="img/filter.png"
        />
        <div className={classes.creatorsHandler}>
          {creators.map((item, index) => {
            return index % 2 === 0 ? (
              <Card
                key={index}
                Creatorname={item.Creatorname}
                Creatorstatus={item.Creatorstatus}
                Creatorimg={item.Creatorimg}
                index={index}
                bg="#51F0B0"
                textcolor="white"
                arrow="/img/arrow-right.png"
              />
            ) : (
              <Card
                key={index}
                Creatorname={item.Creatorname}
                Creatorstatus={item.Creatorstatus}
                Creatorimg={item.Creatorimg}
                index={index}
                bg="white"
                textcolor="#51F0B0"
                arrow="/img/arrow-blue-left.png"
              />
            );
          })}
        </div>
      </div>
      <button className={classes.AddButton}>
        <img src="img/plus.png" alt="" />
      </button>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <br/>
      <Footer Explore={true}/>
    </>
  );
};
