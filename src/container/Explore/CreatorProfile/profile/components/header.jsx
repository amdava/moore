import React from "react";
import classes from "../profile.module.scss";
import { Link } from "react-router-dom";
import { Avatar, Row, Col, Dropdown, Menu } from "antd";
import { PlusCircleOutlined } from "@ant-design/icons";

import { useHistory } from "react-router-dom";
export const Header = ({ enableheader, data }) => {
	const history = useHistory();

	const onMoveBack = () => {
		history.goBack();
		console.log("history", history);
	};

	const NotLoggedmenu = (
		<Menu>
			<Menu.Item key="1">
				<Link to="/login">Login</Link>
			</Menu.Item>
			<Menu.Item key="2">
				<Link to="/register">Register</Link>
			</Menu.Item>
			<Menu.Item key="3">
				<Link to="/forgetpassword">Forget Password</Link>
			</Menu.Item>
		</Menu>
	);

	return (
		<div className={classes.headercontainer}>
			<div className={classes.Header}>
				<div onClick={onMoveBack}>
					<img src="/img/arrow-left.png" alt="" className={classes.arrowleft} />
				</div>
				<div>
					<img src="/img/settings2.png" alt="" className={classes.arrowRight} />
					<Dropdown
						className={classes.arrowRight}
						overlay={NotLoggedmenu}
						placement="bottomCenter"
						trigger={["click"]}
						arrow
					>
						<img src="/img/settings2.png" alt="" />
					</Dropdown>
				</div>
				<Link to="/explore">
					<img src="/img/search.png" alt="" className={classes.search} />
				</Link>
				<div className={classes.bgStyle}>
					<img src={data.cover ? data.cover : "https://www.w3schools.com/howto/img_forest.jpg"} alt="" />
				</div>
				<Avatar
					className={classes.avatarStyle}
					src={
						data.photo
							? data.photo
							: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQBB2V9omR8vXGvd3IekVOr4Y4VPD9OOd_srQ&usqp=CAU"
					}
					alt=""
				></Avatar>

				<div className={classes.text}>
					<span>{data.userName !== "" ? data.userName : "Required Username"}</span>
					<span>Photographer</span>
					<span>{data.state}</span>
				</div>
			</div>
			<Row className={enableheader ? classes.showmobileInfo : classes.mobileInfo} justify="center">
				<Col className={classes.mobileInfoCol}>
					<span>150</span>
					<span>Following</span>
				</Col>
				<Col className={classes.mobileInfoCol}>
					<span>321</span>
					<span>Followers</span>
				</Col>
			</Row>
		</div>
	);
};
