import React, { useState, useEffect } from "react";
import classes from "./profile.module.scss";
import { Header } from "./components/header";
import { Row, Col, Button, Avatar } from "antd";
import { Post } from "./components/posts/post";
import { useSelector } from "react-redux";
import { store } from "../../../../firebase";

export const Profile = () => {
	const [posts, setposts] = useState([]);
	const [top, setTop] = useState(10);

	const { data } = useSelector((state) => state.UserReducer);
	const [tab, settab] = useState(3);

	useEffect(() => {
		store
			.collection("posts")
			.where("email", "==", data.email)
			.onSnapshot((res) => {
				const userPosts = res.docs.map((doc) => doc.data());
				setposts(userPosts);
			});
	}, []);

	const EnableTab = (value) => {
		settab(value);
	};
	const [TabItemActive, setTabItemActive] = useState(1);

	return (
		<div className={`${classes.container} min-h-screen`}>
			<Header enableheader={true} data={data} />
			<Row justify="space-around" className={classes.RowStyle}>
				<Col className={classes.ColStyle}>
					<span style={{ color: "white" }}>Nothing</span>

					<span
						className={classes.heading}
						style={tab === 1 ? { borderBottom: "5px solid #2BC2D3" } : null}
						onClick={() => EnableTab(1)}
					>
						About
					</span>
				</Col>
				<Col className={classes.ColStyle}>
					<span className={classes.status}>1225</span>

					<span
						className={classes.heading}
						style={tab === 2 ? { borderBottom: "5px solid #2BC2D3" } : null}
						onClick={() => EnableTab(2)}
					>
						Subscribers
					</span>
				</Col>
				<Col className={classes.ColStyle}>
					<span className={classes.status}>44</span>

					<span
						className={classes.heading}
						style={tab === 3 ? { borderBottom: "5px solid #2BC2D3" } : null}
						onClick={() => EnableTab(3)}
					>
						Posts
					</span>
				</Col>
			</Row>
			{tab === 2 ? (
				<>
					{posts.map((item, index) => {
						return (
							<div className={classes.subcard} key={index}>
								<Avatar src={item.avatarimg} alt="" className={classes.subcardAvatar} />
								<div className={classes.subcardheading}>{item.Username}</div>
								<div className={classes.subcardtime}>3 hours ago</div>
							</div>
						);
					})}
					<Button className={classes.ButtonStyle}>
						<img src="/img/add-mentor-icon.png" alt="" />
						&nbsp;&nbsp;
						<span>Take Subscriptions</span>
					</Button>
				</>
			) : tab === 1 ? (
				<div className={classes.AboutHandler}>
					<div className={classes.Aboutheading}>
						Sharing my experience with aspiring photographers
					</div>
					<div className={classes.Aboutparagraph}>
						Hello, my name’s Lucy Lee. I’m a photographer from New York, and have been in the
						industry for over 6 years. I specialise in landscape photography, and am passionate
						about sharing everything that I’ve learned on my photographic journey over the last 6
						years. Ultimately, my goal is to help teach you everything about photography and to
						develop your skills from an absolute beginner, to professional photographer.
					</div>
					<div className={classes.Aboutheading}>Key features</div>
					<div className={classes.Aboutparagraph2}>
						<img src="/img/bullet.png" alt="" />
						<div>
							You will learn how to develop your career and make money as a professional
							photographer.
						</div>
					</div>
					<div className={classes.Aboutparagraph2}>
						<img src="/img/bullet.png" alt="" />
						<div>
							You will learn how to take awesome photos that impress your family, friends and
							colleagues.
						</div>
					</div>
					<div className={classes.Aboutparagraph2}>
						<img src="/img/bullet.png" alt="" />
						<div>
							You will learn how to compose images by following the essential rules of photography.
						</div>
					</div>
					<Button className={classes.ButtonStyle}>
						<img src="/img/add-mentor-icon.png" alt="" />
						&nbsp;&nbsp;
						<span>Take Subscriptions</span>
					</Button>
				</div>
			) : (
				<div>
					<div className={classes.tabcontainer}>
						<div className={classes.tabcontent}>
							<div className={classes.tabcard}>
								<div>Free Subscription</div>
								<Button className="flex items-center justify-center"><span>Following</span></Button>
							</div>
							<div className={classes.tabicons}>
								<img src="/img/explore-1.png" alt="" />
								<img src="/img/explore-2.png" alt="" />
								<img src="/img/explore-3.png" alt="" />
								<img src="/img/explore-4.png" alt="" />
							</div>
						</div>
						<div className={classes.tabhandler}>
							<div
								className={TabItemActive === 1 ? classes.postActive : null}
								onClick={() => setTabItemActive(1)}
							>
								<img src="/img/explore-5.png" alt="" />
							</div>
							<div
								className={TabItemActive === 2 ? classes.imgActive : null}
								onClick={() => setTabItemActive(2)}
							>
								<img src="/img/explore-6.png" alt="" />
							</div>
							<div
								className={TabItemActive === 3 ? classes.videoActive : null}
								onClick={() => setTabItemActive(3)}
							>
								<img src="/img/explore-7.png" alt="" />
							</div>
						</div>
					</div>
					{posts.map((item, index) => {
						return <Post key={index} userData={item} />;
					})}
				</div>
			)}
		</div>
	);
};
