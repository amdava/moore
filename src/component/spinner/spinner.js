import React from "react";
import { Spin } from "antd";
import { LoadingOutlined } from "@ant-design/icons";

export const Spinner = (props) => {
  const antIcon = (
    <LoadingOutlined style={{ color: "black", fontSize: 40 }} spin />
  );
  return (
    <Spin indicator={antIcon} size="large" spinning={props.loading}>
      {props.children}
    </Spin>
  );
};
